import mongoose from "mongoose";

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  Price: {
    type: String,
    required: true,
  },
  About_this_items: {
    type: [String],
    required: true,
  },
  Discription: {
    type: String,
    required: true,
  },
  rating: {
    type: Number,
    required: true,
  },
  qty: {
    type: Number,
    required: true,
  },
  Brand: {
    type: String,
    required: true,
  },
  reviews: {
    type: Number,
    required: true,
  },
  color: {
    type: String,
    required: true,
  },
  mainImg: {
    type: String,
    required: true,
  },
  imgList: {
    type: [String],
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
});

const Product = mongoose.model("Product", productSchema);

export default Product;
