import "../css/Ads.css";
import GirlImp from "/images/image_5-removebg-preview 1.png";

function Ads() {
  return (
    <div className='AdsMainDiv'>
      <div className='offer'>
        <p>
          Grab upto 50% off on <br />
          Selected headphones
        </p>
        <button>Buy Now</button>
      </div>
      <img
        src={GirlImp}
        alt='girlImg'
      />
    </div>
  );
}

export default Ads;
