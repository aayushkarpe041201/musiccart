const products = [
  {
    name: "Sony WH-CH720N",
    Price: 3500,
    About_this_items: [
      "Sony’s lightest Wireless Noise-cancelling headband ever",
      "Up to 50-hour battery life with quick charging (3 min charge for up to 1 hour of playback)",
      " Multi-Point Connection helps to pair with two Bluetooth devices at the same time",
      " Take noise cancelling to the next level with Sony’s Integrated Processor V1,so you can fully immerse yourself in the music",
      " Super comfortable and lightweight design ( 192 Grams )",
      " High sound quality and well-balanced sound tuning",
    ],
    Discription:
      "Sony WH-CH720N, Wireless Over-Ear Active Noise Cancellation Headphones with Mic, up to 50 Hours Playtime, Multi-Point Connection, App Support, AUX & Voice Assistant Support for Mobile Phones (Black)",
    rating: 5,
    qty: 10,
    Brand: "Sony",
    reviews: 50,
    color: "Black",
    mainImg: "https://m.media-amazon.com/images/I/61Wiw8zIwvL._SY879_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/71ae7ZXQVdL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/71ae7ZXQVdL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/61Vc2OEc7aL._SX679_.jpg",
    ],
    type: "Over-ear headphone",
  },

  {
    name: "OnePlus Bullets Z2",
    Price: 2229,
    About_this_items: [
      "A quick 10-minute charge delivers up to 20 hours of immersive audio playback.",
      "Frequency Response Range 20Hz - 20,000KHz. Bluetooth Connection range Up to 10 meters",
      "The flagship-level battery life delivers up to 30 hours of non-stop music on a single charge. Codec: Supports AAC and SBC",
      "A large 12.4 mm bass driver delivers uncompromisingly deep bass for powerful beats. Experience incredibly rich audio detail at every frequency with the titanium coating dome",
      "Anti-distortion audio technology ensures your audio playtime stays silky-smooth. Thanks to the larger sound cavity and industry-leading algorithms, notes remain distortion-free",
      "Water and sweat-resistant, the IP55-rated internals and design ensure your OnePlus Bullets Wireless Z2 stay all-weather ready.",
    ],
    Discription:
      "OnePlus Bullets Z2 Bluetooth Wireless in Ear Earphones with Mic, Bombastic Bass, 10 Mins Charge - 20 Hrs Music, 30 Hrs Battery Life (Acoustic Red)",
    rating: 4.2,
    qty: 3,
    Brand: "Oneplus",
    reviews: 100,
    color: "Green",
    mainImg: "https://m.media-amazon.com/images/I/51UhwaQXCpL._SX679_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/51tdhnb9hBL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/41IyaPgXxJL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/61VyK3V84ML._SX679_.jpg",
    ],
    type: "On-ear headphone",
  },

  {
    name: "boAt Rockerz 330",
    Price: 1599,
    About_this_items: [
      "Rockerz 330 Pro offers a massive playback time of up 60HRS on a single full charge",
      "This neckband has our ENx technology that helps to capture your voice without background glitches and lets you be heard crystal clear over voice calls",
      "With our ASAP Charge tech, the neckband can gather up to 20HRS of playtime in just 10Min of charge, Charging Time About 1 hour",
      "Our boAt Signature sound shines through whenever you want to get indulged in your cherished playlists courtesy the 10mm drivers",
      "The ergonomically designed neckband has magnetic earbuds that support easy storage and carry when not in use You can listen to your favourite tracks in a carefree manner, courtesy the protection offered by its IPX5 marked water resistance",
      " You can enjoy the advantage of dual pairing by having the neckband stay connected to two devices simultaneously, for eg: phone and laptop.",
      "1 year warranty from the date of purchase.",
    ],
    Discription:
      "boAt Rockerz 330 Pro in-Ear Bluetooth Neckband with 60HRS Playtime, ASAP Charge, ENx Tech, Signature Sound, BT v5.2, Dual Pairing, IPX5, with Mic (Active Black)",
    rating: 4.7,
    qty: 12,
    Brand: "Boat",
    reviews: 250,
    color: "Black",
    mainImg: "https://m.media-amazon.com/images/I/514FHju3wzL._SX679_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/514FHju3wzL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/61a6twk4A9L._SX679_.jpg",
      "https://m.media-amazon.com/images/I/61JR0DcN0eL._SX679_.jpg",
    ],
    type: "On-ear headphone",
  },

  {
    name: "PHILIPS Wireless Bluetooth",
    Price: 10331,
    About_this_items: [
      "BLUETOOTH 5.0: The latest Bluetooth 5.0 offers enhanced sound transmission, faster pairing, more stable wireless connection and wider compatibility with Bluetooth enabled cell phones, tablets, computers, smart watches and smart speakers",
      "IPX5 WATERPROOF: Designed for your active lifestyle. These earplugs are IPX 5 and can withstand sweating. The ergonomic design provides the ultimate in comfort for your active lifestyle, such as running, jogging, cycling, driving, camping, hiking, fitness and other outdoor sports.",
      "CALL VIBRATION: Philips Bluetooth headphones will vibrate when a call comes in so that you won't miss any calls even if you’re deep into your music or have the earbuds around your neck. No more of those annoying echoes when you are talking on the phone. With our acoustic echo cancellation, you always get a clear, undisturbed connection.",
      "POWERFUL BASS, RAPID CHARGE: Perfectly tuned neodymium acoustic drivers deliver deep bass and clear midrange frequency. With our acoustic echo cancellation, you always get a clear, undisturbed connection. If you need even more power, just 5 minutes on charge will give you another 2 hours play.",
      "COMFORTABLE FIT, INTERCHANGEABLE EARBUD COVERS: The slender neckband sits comfortably behind your neck. An oval acoustic tube and three sizes of interchangeable rubber earbud covers create a perfect seal. Enjoy all-day listening comfort and excellent passive noise isolation.",
    ],
    Discription:
      "PHILIPS Wireless Bluetooth In Ear Neckband Earphone with Mic (Black)",
    rating: 4.9,
    qty: 5,
    Brand: "Philips",
    reviews: 100,
    color: "Blue",
    mainImg: "https://m.media-amazon.com/images/I/718HvK+RQNL._SX679_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/91pVZdvKHuL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/91pVZdvKHuL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/71Zy4LdnezL._SX679_.jpg",
    ],
    type: "In-ear headphone",
  },

  {
    name: "LG Electronics Wireless Bluetooth Headset",
    Price: 10248,
    About_this_items: [
      "BLUETOOTH 5.0: The latest Bluetooth 5.0 offers enhanced sound transmission, faster pairing, more stable wireless connection and wider compatibility with Bluetooth enabled cell phones, tablets, computers, smart watches and smart speakers",
      "IPX5 WATERPROOF: Designed for your active lifestyle. These earplugs are IPX 5 and can withstand sweating. The ergonomic design provides the ultimate in comfort for your active lifestyle, such as running, jogging, cycling, driving, camping, hiking, fitness and other outdoor sports.",
      "CALL VIBRATION: Philips Bluetooth headphones will vibrate when a call comes in so that you won't miss any calls even if you’re deep into your music or have the earbuds around your neck. No more of those annoying echoes when you are talking on the phone. With our acoustic echo cancellation, you always get a clear, undisturbed connection.",
      "POWERFUL BASS, RAPID CHARGE: Perfectly tuned neodymium acoustic drivers deliver deep bass and clear midrange frequency. With our acoustic echo cancellation, you always get a clear, undisturbed connection. If you need even more power, just 5 minutes on charge will give you another 2 hours play.",
      "COMFORTABLE FIT, INTERCHANGEABLE EARBUD COVERS: The slender neckband sits comfortably behind your neck. An oval acoustic tube and three sizes of interchangeable rubber earbud covers create a perfect seal. Enjoy all-day listening comfort and excellent passive noise isolation.",
    ],
    Discription: "LG TONE Triumph Black",
    rating: 4.8,
    qty: 11,
    Brand: "LG Electronics",
    reviews: 120,
    color: "Blue",
    mainImg: "https://m.media-amazon.com/images/I/61eezcHMyUL._SX679_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/61eezcHMyUL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/61eezcHMyUL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/61eezcHMyUL._SX679_.jpg",
    ],
    type: "In-ear headphone",
  },

  {
    name: "Immortal 121",
    Price: 3490,
    About_this_items: [
      " Power Up & Play Enjoy uninterrupted play with 40 Hours of playback. In case you run out of charge at a crucial moment, just pause and charge for 10 Minutes to play and win for 180 Minutes with our ASAP Charge",
      "BEAST™️ Mode Up your level with quicker gaming response. The super low latency and smooth syncing of our BEAST™️ Mode delivers sound almost instantaneously so that you can respond to the sounds in the game better and quicker",
      "Blazing RGB Lights Get in the right vibe with Immortal 121’s blazing RGB lights. Level up your style game with lights that make you and your Immortal 121 truely stand apart",
      "Clear Communication. Clear Sound Communicate with your team with clarity and outstanding sound. Its Quad Mics with ENx™️ Technology captures only your voice, eliminating background sound so that you can communicate clearly during team missions. And boAt Signature Sound makes the beats come alive to capture every minute detail for the ultimate gaming experience",
      "Insta Wake N’ Pair Technology Forget the hassle of manually connecting your device and get into gaming mode instantaneously with Insta Wake N’ Pair Technology.Immortal 121 connects with your paired device as soon as you open its case lid. So let the game begin!",
    ],
    Discription:
      "Bluetooth Gaming Wireless Earbuds with BEAST™️ Mode (40ms Low Latency), ASAP™️ Charge, 40 Hours Playback, & Blazing RGB Lights",
    rating: 5,
    qty: 16,
    Brand: "Boat",
    reviews: 100,
    color: "White",
    mainImg: "https://m.media-amazon.com/images/I/61q-2yzbBtL._SX679_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/81C4li-v9qL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/617MZLntFiL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/61glRpWr99L._SX679_.jpg",
    ],
    type: "In-ear headphone",
  },

  {
    name: "Samsung Galaxy Buds2 Pro",
    Price: 19999,
    About_this_items: [
      "24-bit Hi-Fi audio. The upgraded Samsung Seamless Codec encodes the full 24-bit audio and is decoded via Galaxy Buds2 Pro, maintaining that same 24-bit high-quality sound. (Samsung Galaxy device with One UI version 4.0 or higher required for 24-bit audio)",
      "Intelligent ANC puts your playlist in focus. With 3 high SNR (Signal-to-Noise Ratio) microphones, Galaxy Buds2 Pro track and eliminate more outside sound — even soft sounds like wind. Switching to in-person conversations is easy with Voice Detect. Simply start talking and Voice Detect will turn off ANC and activate Ambient sound — allowing you to clearly hear the conversation without removing your Buds2 Pro",
      "Comfort fit. We redesigned the earbuds to be 15% smaller than Buds Pro to fit snugly in your ear. Enjoy a comfortable fit, thanks to the ergonomically designed earbuds. Enhanced wind flow technology helps reduce ear canal pressure and minimizes noise — for comfort and clarity",
      "360 AUDIO - Hear it like you're there. With Intelligent 360 Audio, the sound feels more realistic.The 360 Audio algorithms with Direct multichannel (5.1ch / 7.1ch / Dolby Atmos) and Enhanced Dolby Head Tracking make every movement stay in sync — creating an immersive experience. 360 Audio can even pinpoint the direction of the sound as you move your head, giving you the spatial experience",
      "Get up to 5 hrs of continuous playtime with ANC on and up to 18 hrs in the cradle. Galaxy Buds2 Pro run on Bluetooth v5.3 and are IPX7 water resistant",
    ],
    Discription:
      "Samsung Galaxy Buds2 Pro, Bluetooth Truly Wireless in Ear Earbuds with Noise Cancellation (Graphite, with Mic)",
    rating: 4.5,
    qty: 12,
    Brand: "SAMSUNG",
    reviews: 500,
    color: "White",
    mainImg:
      "https://m.media-amazon.com/images/I/31IUpZAySVL._SX300_SY300_QL70_FMwebp_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/31IUpZAySVL._SX300_SY300_QL70_FMwebp_.jpg",
      "https://m.media-amazon.com/images/I/71BptsQUN5L._SX679_.jpg",
      "https://m.media-amazon.com/images/I/71vLKhd9T8L._SX679_.jpg",
    ],
    type: "In-ear headphone",
  },

  {
    name: "Jabra Elite 10 True Wireless Earbuds",
    Price: 20000,
    About_this_items: [
      "INCREDIBLY COMFORTABLE – Sink into all-day comfort with Jabra ComfortFit technology. The semi-open design on these Elite 10 wireless earbuds removes the feeling of having your ears plugged, and the unique EarGel design helps relieve in-ear pressure",
      "ULTIMATE SOUND – With Elite 10 you get Spatial Sound with Dolby Head Tracking technology for a more immersive Dolby Atmos experience. Complete with 10mm speakers, the lifelike audio in these Jabra earbuds gives your sound new clarity",
      "2X STRONGER NOISE CANCELLATION – These Jabra Advanced Active Noise Cancelling earbuds automatically adjusts to fit every ear canal and the environment, so you can get in the zone and block out everything, from coffee shop buzz to train noise",
      "CLEAR CALLS EVEN IN NOISY ENVIRONMENTS – 6-Mic technology picks up your voice and not the arounds sounds around you, even when outdoors. Featuring a long lasting battery life with - up to 6 hours playback and 27 hours with the wireless charging case, plus a fast charge option",
      "UNIVERSAL CONNECTIVITY – Bluetooth Multipoint lets you connect to 2 devices at a time so you can switch from your laptop to phone without missing a beat. Elite 10 Bluetooth wireless earphones work with iOS, Android, PC, tablets and more",
    ],
    Discription:
      "Jabra Elite 10 True Wireless Earbuds – Advanced Active Noise Cancelling Earbuds with Next-Level Dolby Atmos Surround Sound –All-Day Comfort, Multipoint Bluetooth, Wireless Charging - Gloss Black",
    rating: 5,
    qty: 19,
    Brand: "JABRA",
    reviews: 4,
    color: "White",
    mainImg:
      "https://m.media-amazon.com/images/I/31csvs3C6LL._SX300_SY300_QL70_FMwebp_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/519OYJbFqfL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/71Kqf-QjTyL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/7167OJZBcXL._SX679_.jpg",
    ],
    type: "In-ear headphone",
  },

  {
    name: "JBL Tune 760NC",
    Price: 7999,
    About_this_items: [
      "ACTIVE NOISE CANCELLATION: Tune out the Noise & Tune in to your Music with JBL Tune 760NC wireless headset",
      "JBL PURE BASS SOUND: For over 70 years, JBL has engineered the precise, impressive sound found in big venues around the world. These Bluetooth headphones reproduce that same JBL sound, punching out bass that’s both deep and powerful",
      "UP TO 50 HOURS PLAYTIME: Listen wirelessly for up to 35 hours with Active Noise Cancellation or up to 50 hours without Noise Cancellation for long-lasting fun. Recharge the battery quickly in 2 hours or enjoy endlessly in wired mode using the detachable AUX cable provided",
      "GOOGLE FAST PAIR: Thanks to Fast Pair enabled by Google, the JBL Tune 760NC are immediately connected to your Android device. You can also pair multiple devices just by tapping on the devices’ screen",
      "DUAL PAIRING (MULTI-POINT CONNECTION): Allows you to effortlessly switch from one Bluetooth device to another. You can simply switch from a video on your tablet to a call on your mobile phone so that you’ll never miss a call",
    ],
    Discription:
      "JBL Tune 760NC, Wireless Over Ear Active Noise Cancellation Headphones with Mic, up to 50 Hours Playtime, Pure Bass, Dual Pairing, AUX & Voice Assistant Support for Mobile Phones",
    rating: 5,
    qty: 14,
    Brand: "JBL",
    reviews: 4.5,
    color: "Black",
    mainImg:
      "https://m.media-amazon.com/images/I/41machJsMZL._SX300_SY300_QL70_FMwebp_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/71ae7ZXQVdL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/61xtto8AvRL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/61Vc2OEc7aL._SX679_.jpg",
    ],
    type: "Over-ear headphone",
  },

  {
    name: "OnePlus Buds Pro 2",
    Price: 13900,
    About_this_items: [
      "Seamless connectivity: Instantaneous pairing with OnePlus smartphone, as soon as you open the charging case lid. It also has Google Fast Pair that helps to connect any android phones easily. To use earbuds functions, please upgrade the Wireless Earphones APP or OnePlus Buds APP to the latest version from Google Play",
      "Spatial audio: Enjoy Spatial Audio with Dynamic Head Tracking feature that helps you experience sounds on a whole new level. Co-created with Dynaudio: Helps you achieve optimal listening experience. Smart Scene Noise Cancellation2.0 : Up to 48dB Ultra-wide frequency noise cancellation system with personalized ANC",
      "Worry-free Battery Life: Up-to 40 Hours Battery with quick charging 10 minutes for 3 hours of use with ANC on. Full charged earbud: up to 6h(music playback; ANC ON); up to 9h(music playback; ANC Off). OnePlus Audio ID2.0 : Personalize your audio via simple listening test at first time setup. This unique hearing profile ID allows one to enjoy the most optimal listening experience best suited to him or her by compensating for mild hearing loss and related aural deficiencies",
      "Powerful, Punchy Bass with clear vocals: Drop the bass and listen to clear vocals with dual 11mm + 6mm dual drivers, delivering a precisely boosted sound profile designed to move you",
      "3Mic AI for better call and noise cancellation. IP55 Water, Dust and Sweat resistant",
    ],
    Discription:
      "OnePlus Buds Pro 2 Bluetooth Truly Wireless in Ear Earbuds with Spatial Audio Dynamic Head Tracking,co-Created with Dynaudio,Upto 48dB Adaptive Noise Cancellation,Upto 40Hrs Battery",
    rating: 4,
    qty: 31,
    Brand: "Oneplus",
    reviews: 100,
    color: "Black",
    mainImg: "https://m.media-amazon.com/images/I/61CeRMMV7+L._SX679_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/61lEfUv8gbL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/61-p5iMBC5L._SX679_.jpg",
      "https://m.media-amazon.com/images/I/71UMrTcV3NL._SX679_.jpg",
    ],
    type: "On-ear headphone",
  },

  {
    name: "boAt BassHeads 100",
    Price: 329,
    About_this_items: [
      "The perfect way to add some style and stand out from the crowd with the boAt BassHeads 100 Hawk inspired earphones",

      "The stylish BassHeads 100 superior coated wired earphones are a definite fashion statement - wear your attitude with its wide variety of collection",

      "The powerful 10mm dynamic driver with the speaker resistance of 16 ohm enables the earphone to deliver a punchy, rhythmic response to the most demanding tracks",

      "Features a HD microphone to make crystal clear calls - making life easier on the go, play/pause music or answer/end calls with one-click, track forward with two-clicks or back with 3 clicks make these earphones extremely user-friendly",

      "With 1.2 meters perfect length cable now plug it in anywhere with ease - while the earphones are extremely user-friendly and stylish, extruding premium coating on the wire cable is a manufacturing process that we've been performing and refining since our company's inception",

      "Its impeccable fit won't let you take these wonderfully amazing earphones off, fits you perfectly without hurting your ears and the perfect arc, with different size ear tips helps you in achieving superior comfort even with long listening hours.",
      "1 year warranty from the date of purchase",
    ],
    Discription: "boAt BassHeads 100 in-Ear Wired Headphones with Mic (Black)",
    rating: 4.1,
    qty: 21,
    Brand: "Boat",
    reviews: 40,
    color: "Black",
    mainImg:
      "https://m.media-amazon.com/images/I/31Inu6y9X2L._SX300_SY300_QL70_FMwebp_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/81nKL4v5EiL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/81+pCe-PR8L._SX679_.jpg",
      "https://m.media-amazon.com/images/I/81XnLhAEnDL._SX679_.jpg",
    ],
    type: "On-ear headphone",
  },

  {
    name: "realme Buds 2",
    Price: 599,
    About_this_items: [
      "Enjoy the powerful 11.2mm bass boost driver which consist of multi-layer composite diaphragm, bringing you a deep and powerful, yet accurate bass response.",

      "The inline remote features three tactile buttons and a mic, so you can control your music and videos, incoming calls, and even summon your voice assistant directly at the touch of a button.",

      "A premium, reinforced braided jacket and two evenly grooved TPU cables make for a design that is robust and durable.",

      "A premiun neckband design along with a tangle free memory metal string ensures a great experience.",

      "Add a touch of style to your music experience with the realme Buds 2. The matte, streamlined design looks elegant and attractive. Connector type: 3.5 mm6 months warranty",
      "country of Origin: China]",
    ],
    Discription: "realme Buds 2 Wired in Ear Earphones with Mic (Green)",
    rating: 2,

    qty: 30,
    Brand: "realme",
    reviews: 50,
    color: "Green",
    mainImg: "https://m.media-amazon.com/images/I/61ZEQXGTepL._SX679_.jpg",
    imgList: [
      "https://m.media-amazon.com/images/I/71jbCnynKjL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/713+yVJCDbL._SX679_.jpg",
      "https://m.media-amazon.com/images/I/6127HXUu4hL._SX679_.jpg",
    ],
    type: "On-ear headphone",
  },
];

export default products;
