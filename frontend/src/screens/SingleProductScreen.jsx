import "../css/SingleProductScreen.css";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { listProductDetails } from "../actions/productAction";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Loder from "../loder/Loder";
import PropTypes from "prop-types";
import WebHeadingNCartBtn from "../components/WebHeadingNCartBtn";
import { addToCart } from "../actions/cartActions";
import { useState } from "react";

const CirclesComponent = ({ length, currentSlide }) => {
  // Array to store circle components
  const circles = [];

  // Loop to generate circles
  for (let i = 0; i < length; i++) {
    // Determine the fill color based on the currentSlide
    const fillColor = i === currentSlide ? "#2E0052" : "white";

    // Push the circle component to the array
    circles.push(
      <circle
        key={i}
        cx={`${i * 20 + 6}`} // Adjust the positioning based on your requirement
        cy='6'
        r='5.25'
        fill={fillColor}
        stroke='#A1A1A1'
        strokeWidth='1.5'
      />
    );
  }

  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width={length * 20 + 6}
      height='12'
      viewBox={`0 0 ${length * 20 + 6} 12`}
      fill='none'>
      {circles}
    </svg>
  );
};

function SingleProductScreen({ isMobile }) {
  let navigate = useNavigate();
  const dispatch = useDispatch();

  const { id } = useParams();

  const productDetails = useSelector((state) => state.productDetails);
  const { loading, error, product } = productDetails;

  const addToCartHandler = () => {
    dispatch(addToCart(product._id, 1));
    navigate(`/cart/${id}`);
  };

  useEffect(() => {
    dispatch(listProductDetails(id));

    if (error) {
      toast.error(error);
    }
  }, [id, dispatch, error]);

  const [currentslide, setCurrentslide] = useState(0);

  const [length, setLength] = useState(0);
  const [sliderImges, setSliderImges] = useState([]);

  useEffect(() => {
    if (product && product.imgList && product.mainImg) {
      const sliderImges = [...new Set([product.mainImg, ...product.imgList])];
      let length = sliderImges.length;

      setSliderImges(sliderImges);
      setLength(length);
    }
  }, [product]);

  const nextSlide = () => {
    setCurrentslide(currentslide === length - 1 ? 0 : currentslide + 1);
  };

  const prevSlide = () => {
    setCurrentslide(currentslide === 0 ? length - 1 : currentslide - 1);
  };

  return isMobile ? (
    <>
      {loading ? (
        <>
          <WebHeadingNCartBtn route={`/loding..`} />
          <svg
            onClick={() => navigate(-1)}
            className='BackBtnM'
            xmlns='http://www.w3.org/2000/svg'
            width='40'
            height='40'
            viewBox='0 0 40 40'
            fill='none'>
            <g filter='url(#filter0_d_47_1381)'>
              <circle
                cx='20'
                cy='19'
                r='16'
                fill='white'
              />
            </g>
            <path
              d='M18.7 24.275C18.9 24.075 18.996 23.8334 18.988 23.55C18.98 23.2667 18.8757 23.025 18.675 22.825L15.85 20H27C27.2833 20 27.521 19.904 27.713 19.712C27.905 19.52 28.0007 19.2827 28 19C28 18.7167 27.904 18.479 27.712 18.287C27.52 18.095 27.2827 17.9994 27 18H15.85L18.7 15.15C18.9 14.95 19 14.7124 19 14.437C19 14.1617 18.9 13.9244 18.7 13.725C18.5 13.525 18.2623 13.425 17.987 13.425C17.7117 13.425 17.4743 13.525 17.275 13.725L12.7 18.3C12.6 18.4 12.529 18.5084 12.487 18.625C12.445 18.7417 12.4243 18.8667 12.425 19C12.425 19.1334 12.446 19.2584 12.488 19.375C12.53 19.4917 12.6007 19.6 12.7 19.7L17.3 24.3C17.4833 24.4834 17.7123 24.575 17.987 24.575C18.2617 24.575 18.4993 24.475 18.7 24.275Z'
              fill='black'
            />
            <defs>
              <filter
                id='filter0_d_47_1381'
                x='0'
                y='0'
                width='40'
                height='40'
                filterUnits='userSpaceOnUse'
                colorInterpolationFilters='sRGB'>
                <feFlood
                  floodOpacity='0'
                  result='BackgroundImageFix'
                />
                <feColorMatrix
                  in='SourceAlpha'
                  type='matrix'
                  values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
                  result='hardAlpha'
                />
                <feOffset dy='1' />
                <feGaussianBlur stdDeviation='2' />
                <feComposite
                  in2='hardAlpha'
                  operator='out'
                />
                <feColorMatrix
                  type='matrix'
                  values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.33 0'
                />
                <feBlend
                  mode='normal'
                  in2='BackgroundImageFix'
                  result='effect1_dropShadow_47_1381'
                />
                <feBlend
                  mode='normal'
                  in='SourceGraphic'
                  in2='effect1_dropShadow_47_1381'
                  result='shape'
                />
              </filter>
            </defs>
          </svg>
          <ToastContainer />
        </>
      ) : error ? (
        <>
          <WebHeadingNCartBtn route={`/error!`} />
          <svg
            onClick={() => navigate(-1)}
            className='BackBtnM'
            xmlns='http://www.w3.org/2000/svg'
            width='40'
            height='40'
            viewBox='0 0 40 40'
            fill='none'>
            <g filter='url(#filter0_d_47_1381)'>
              <circle
                cx='20'
                cy='19'
                r='16'
                fill='white'
              />
            </g>
            <path
              d='M18.7 24.275C18.9 24.075 18.996 23.8334 18.988 23.55C18.98 23.2667 18.8757 23.025 18.675 22.825L15.85 20H27C27.2833 20 27.521 19.904 27.713 19.712C27.905 19.52 28.0007 19.2827 28 19C28 18.7167 27.904 18.479 27.712 18.287C27.52 18.095 27.2827 17.9994 27 18H15.85L18.7 15.15C18.9 14.95 19 14.7124 19 14.437C19 14.1617 18.9 13.9244 18.7 13.725C18.5 13.525 18.2623 13.425 17.987 13.425C17.7117 13.425 17.4743 13.525 17.275 13.725L12.7 18.3C12.6 18.4 12.529 18.5084 12.487 18.625C12.445 18.7417 12.4243 18.8667 12.425 19C12.425 19.1334 12.446 19.2584 12.488 19.375C12.53 19.4917 12.6007 19.6 12.7 19.7L17.3 24.3C17.4833 24.4834 17.7123 24.575 17.987 24.575C18.2617 24.575 18.4993 24.475 18.7 24.275Z'
              fill='black'
            />
            <defs>
              <filter
                id='filter0_d_47_1381'
                x='0'
                y='0'
                width='40'
                height='40'
                filterUnits='userSpaceOnUse'
                colorInterpolationFilters='sRGB'>
                <feFlood
                  floodOpacity='0'
                  result='BackgroundImageFix'
                />
                <feColorMatrix
                  in='SourceAlpha'
                  type='matrix'
                  values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
                  result='hardAlpha'
                />
                <feOffset dy='1' />
                <feGaussianBlur stdDeviation='2' />
                <feComposite
                  in2='hardAlpha'
                  operator='out'
                />
                <feColorMatrix
                  type='matrix'
                  values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.33 0'
                />
                <feBlend
                  mode='normal'
                  in2='BackgroundImageFix'
                  result='effect1_dropShadow_47_1381'
                />
                <feBlend
                  mode='normal'
                  in='SourceGraphic'
                  in2='effect1_dropShadow_47_1381'
                  result='shape'
                />
              </filter>
            </defs>
          </svg>
          <ToastContainer />
        </>
      ) : (
        <>
          <div className='SingleProductScreenMainDivMobile'>
            <svg
              onClick={() => navigate(-1)}
              className='BackBtnM'
              xmlns='http://www.w3.org/2000/svg'
              width='40'
              height='40'
              viewBox='0 0 40 40'
              fill='none'>
              <g filter='url(#filter0_d_47_1381)'>
                <circle
                  cx='20'
                  cy='19'
                  r='16'
                  fill='white'
                />
              </g>
              <path
                d='M18.7 24.275C18.9 24.075 18.996 23.8334 18.988 23.55C18.98 23.2667 18.8757 23.025 18.675 22.825L15.85 20H27C27.2833 20 27.521 19.904 27.713 19.712C27.905 19.52 28.0007 19.2827 28 19C28 18.7167 27.904 18.479 27.712 18.287C27.52 18.095 27.2827 17.9994 27 18H15.85L18.7 15.15C18.9 14.95 19 14.7124 19 14.437C19 14.1617 18.9 13.9244 18.7 13.725C18.5 13.525 18.2623 13.425 17.987 13.425C17.7117 13.425 17.4743 13.525 17.275 13.725L12.7 18.3C12.6 18.4 12.529 18.5084 12.487 18.625C12.445 18.7417 12.4243 18.8667 12.425 19C12.425 19.1334 12.446 19.2584 12.488 19.375C12.53 19.4917 12.6007 19.6 12.7 19.7L17.3 24.3C17.4833 24.4834 17.7123 24.575 17.987 24.575C18.2617 24.575 18.4993 24.475 18.7 24.275Z'
                fill='black'
              />
              <defs>
                <filter
                  id='filter0_d_47_1381'
                  x='0'
                  y='0'
                  width='40'
                  height='40'
                  filterUnits='userSpaceOnUse'
                  colorInterpolationFilters='sRGB'>
                  <feFlood
                    floodOpacity='0'
                    result='BackgroundImageFix'
                  />
                  <feColorMatrix
                    in='SourceAlpha'
                    type='matrix'
                    values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
                    result='hardAlpha'
                  />
                  <feOffset dy='1' />
                  <feGaussianBlur stdDeviation='2' />
                  <feComposite
                    in2='hardAlpha'
                    operator='out'
                  />
                  <feColorMatrix
                    type='matrix'
                    values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.33 0'
                  />
                  <feBlend
                    mode='normal'
                    in2='BackgroundImageFix'
                    result='effect1_dropShadow_47_1381'
                  />
                  <feBlend
                    mode='normal'
                    in='SourceGraphic'
                    in2='effect1_dropShadow_47_1381'
                    result='shape'
                  />
                </filter>
              </defs>
            </svg>

            <button
              className='buyBtnM'
              onClick={addToCartHandler}>
              Buy Now
            </button>

            <div className='imageDivM'>
              {!Array.isArray(sliderImges) || sliderImges.length <= 0 ? (
                <></>
              ) : (
                <img
                  className='imageDivMIMG'
                  src={sliderImges[currentslide]}
                  alt='!'
                />
              )}
            </div>

            <div className='sliderDiv'>
              {/* <- */}
              <svg
                onClick={prevSlide}
                xmlns='http://www.w3.org/2000/svg'
                width='11'
                height='10'
                viewBox='0 0 11 10'
                fill='none'>
                <path
                  d='M1.62654 4.71535C1.17816 4.93108 1.17816 5.56957 1.62654 5.7853L8.74294 9.20918C9.13708 9.39881 9.594 9.11159 9.594 8.6742V1.82645C9.594 1.38906 9.13708 1.10184 8.74294 1.29147L1.62654 4.71535Z'
                  fill='white'
                  stroke='#A1A1A1'
                  strokeWidth='1.31249'
                />
              </svg>
              {/* o o o o */}
              <CirclesComponent
                length={length}
                currentSlide={currentslide}
              />
              {/* -> */}
              <svg
                onClick={nextSlide}
                xmlns='http://www.w3.org/2000/svg'
                width='11'
                height='10'
                viewBox='0 0 11 10'
                fill='none'>
                <path
                  d='M9.37346 4.71462C9.82184 4.93035 9.82184 5.56884 9.37346 5.78457L2.25706 9.20844C1.86292 9.39808 1.406 9.11086 1.406 8.67347V1.82572C1.406 1.38833 1.86292 1.10111 2.25706 1.29074L9.37346 4.71462Z'
                  fill='white'
                  stroke='#A1A1A1'
                  strokeWidth='1.31249'
                />
              </svg>
            </div>

            <div className='infoDivM'>
              <p className='pNameM txtInfoM'>{product.name}</p>
              <p className='crM txtInfoM'>
                {" "}
                {"("}
                {product.reviews}
                {" Customer reviews)"}
              </p>
              <p className='discriptionMv txtInfoM'>{product.Discription}</p>
              <p className='atiMv txtInfoM'>About this item</p>
              <ul style={{ listStyleType: "disc" }}>
                {Array.isArray(product.About_this_items) &&
                  product.About_this_items.map((info, index) => (
                    <li
                      key={index}
                      className='txtInfoM'
                      style={{ marginLeft: "2rem" }}>
                      {info}
                    </li>
                  ))}
              </ul>

              {product.qty > 0 ? (
                <>
                  <p className='InM txtInfoM'>
                    <span style={{ fontWeight: "600" }}>Available</span> - In
                    stock
                  </p>
                </>
              ) : (
                <>
                  <p className='InM txtInfoM'>
                    <span style={{ fontWeight: "600" }}>Unavailable</span> - Out
                    of stock
                  </p>
                </>
              )}

              <p className='InM txtInfoM'>
                <span style={{ fontWeight: "600" }}>Brand</span> -{" "}
                {product.Brand}
              </p>
            </div>

            <button
              className='buyBtnM addM'
              onClick={addToCartHandler}>
              Add to cart
            </button>
            <button
              className='buyBtnM finalbtn'
              onClick={addToCartHandler}>
              Buy Now
            </button>
          </div>
        </>
      )}
    </>
  ) : (
    <>
      {loading ? (
        <>
          <WebHeadingNCartBtn route={`/loding...`} />
          <div className='SingleProductScreenMainDiv'>
            <button
              className='backBtn'
              onClick={() => navigate(-1)}>
              Back to products
            </button>
          </div>
          <Loder />
          <ToastContainer />
        </>
      ) : error ? (
        <>
          <WebHeadingNCartBtn route={`/error!`} />
          <div className='SingleProductScreenMainDiv'>
            <button
              className='backBtn'
              onClick={() => navigate(-1)}>
              Back to products
            </button>
          </div>
          <ToastContainer />
        </>
      ) : (
        <>
          <WebHeadingNCartBtn route={`/${product.name}`} />
          <div className='SingleProductScreenMainDiv'>
            <button
              className='backBtn'
              onClick={() => navigate(-1)}>
              Back to products
            </button>

            <p className='singleProductDiscription'>{product.Discription}</p>

            <div className='detailsMainDiv'>
              <div className='imageDiv'>
                <img
                  src={product.mainImg}
                  alt='MainImg'
                />
              </div>
              <div className='DetailsMain'>
                <p className='productName'>{product.name}</p>
                <p className='detailsText'>
                  {"("}
                  {product.reviews}
                  {" Customer reviews)"}
                </p>
                <p className='detailsText priceDet'>
                  Price - ₹ {product.Price}
                </p>

                <p className='detailsText'>
                  {product.color} | Over-ear headphone
                </p>

                <p className='detailsText'>About this item</p>
                <ul style={{ listStyleType: "disc" }}>
                  {Array.isArray(product.About_this_items) &&
                    product.About_this_items.map((info, index) => (
                      <li
                        key={index}
                        className='detailsText'
                        style={{ marginLeft: "2rem" }}>
                        {info}
                      </li>
                    ))}
                </ul>
                {product.qty > 0 ? (
                  <>
                    <p className='detailsText'>
                      <span style={{ fontWeight: "600" }}>Available</span> - In
                      stock
                    </p>
                  </>
                ) : (
                  <>
                    <p className='detailsText'>
                      <span style={{ fontWeight: "600" }}>Unavailable</span> -
                      Out of stock
                    </p>
                  </>
                )}

                <p className='detailsText'>
                  <span style={{ fontWeight: "600" }}>Brand</span> -{" "}
                  {product.Brand}
                </p>
              </div>
            </div>

            <div className='addtopcartSection'>
              <div className='disImgs'>
                {Array.isArray(product.imgList) &&
                  product.imgList.map((link, index) => (
                    <img
                      key={index}
                      src={link}
                      alt='img'
                    />
                  ))}
              </div>

              <div className='atcNbnBtns'>
                <button
                  className='addToCartbtn'
                  disabled={product.qty === 0}
                  onClick={addToCartHandler}>
                  Add to cart
                </button>
                <button
                  className='buyNowbtn'
                  disabled={product.qty === 0}
                  onClick={addToCartHandler}>
                  Buy Now
                </button>
              </div>
            </div>
            <ToastContainer />
          </div>
        </>
      )}
    </>
  );
}
SingleProductScreen.propTypes = {
  isMobile: PropTypes.bool.isRequired,
};

CirclesComponent.propTypes = {
  length: PropTypes.number.isRequired,
  currentSlide: PropTypes.number.isRequired,
};

export default SingleProductScreen;
