import { useNavigate } from "react-router-dom";
import WebHeadingNCartBtn from "../components/WebHeadingNCartBtn";
import "../css/CartScreen.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import mainImg from "../../public/images/image 23.jpg";

import cartLog from "../../public/images/Vector.png";
import { useDispatch, useSelector } from "react-redux";

import { useEffect } from "react";

import { addToCart, removeFromCart } from "../actions/cartActions";
import PropTypes from "prop-types";

function CartScreen({ isMobile }) {
  let navigate = useNavigate();
  const dispatch = useDispatch();

  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;

  useEffect(() => {
    if (cartItems.length === 0) {
      toast.warn("Your cart is empty");
    }
  }, [cartItems]);

  const removeFromCartHandler = (id) => {
    dispatch(removeFromCart(id));
  };

  const checkoutHandler = () => {
    navigate(`/login?redirect=/checkout`);
  };

  return isMobile ? (
    <>
      <div className='cartScreenDviewMain'>
        <button
          style={{ marginTop: "5rem" }}
          className='backBtn'
          onClick={() => navigate("/")}>
          Back to products
        </button>
      </div>

      {cartItems.length === 0 ? (
        <>
          <ToastContainer />
        </>
      ) : (
        <>
          <div className='cartScrrenMVMain'>
            <svg
              className='csbtnmv'
              onClick={() => navigate("/")}
              xmlns='http://www.w3.org/2000/svg'
              width='40'
              height='40'
              viewBox='0 0 40 40'
              fill='none'>
              <g filter='url(#filter0_d_55_1412)'>
                <circle
                  cx='20'
                  cy='19'
                  r='16'
                  fill='white'
                />
              </g>
              <path
                d='M18.7 24.275C18.9 24.075 18.996 23.8334 18.988 23.55C18.98 23.2667 18.8757 23.025 18.675 22.825L15.85 20H27C27.2833 20 27.521 19.904 27.713 19.712C27.905 19.52 28.0007 19.2827 28 19C28 18.7167 27.904 18.479 27.712 18.287C27.52 18.095 27.2827 17.9994 27 18H15.85L18.7 15.15C18.9 14.95 19 14.7124 19 14.437C19 14.1617 18.9 13.9244 18.7 13.725C18.5 13.525 18.2623 13.425 17.987 13.425C17.7117 13.425 17.4743 13.525 17.275 13.725L12.7 18.3C12.6 18.4 12.529 18.5084 12.487 18.625C12.445 18.7417 12.4243 18.8667 12.425 19C12.425 19.1334 12.446 19.2584 12.488 19.375C12.53 19.4917 12.6007 19.6 12.7 19.7L17.3 24.3C17.4833 24.4834 17.7123 24.575 17.987 24.575C18.2617 24.575 18.4993 24.475 18.7 24.275Z'
                fill='black'
              />
              <defs>
                <filter
                  id='filter0_d_55_1412'
                  x='0'
                  y='0'
                  width='40'
                  height='40'
                  filterUnits='userSpaceOnUse'
                  colorInterpolationFilters='sRGB'>
                  <feFlood
                    floodOpacity='0'
                    result='BackgroundImageFix'
                  />
                  <feColorMatrix
                    in='SourceAlpha'
                    type='matrix'
                    values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
                    result='hardAlpha'
                  />
                  <feOffset dy='1' />
                  <feGaussianBlur stdDeviation='2' />
                  <feComposite
                    in2='hardAlpha'
                    operator='out'
                  />
                  <feColorMatrix
                    type='matrix'
                    values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.33 0'
                  />
                  <feBlend
                    mode='normal'
                    in2='BackgroundImageFix'
                    result='effect1_dropShadow_55_1412'
                  />
                  <feBlend
                    mode='normal'
                    in='SourceGraphic'
                    in2='effect1_dropShadow_55_1412'
                    result='shape'
                  />
                </filter>
              </defs>
            </svg>

            <div className='listcartproductsMvDiv'>
              {cartItems.map((item) => (
                <div
                  className='itemDivmv'
                  key={item.product}>
                  <div className='imgDivCartMv'>
                    <img
                      src={item.img}
                      alt='!!'
                    />
                  </div>

                  <div className='infoDivCartMv'>
                    <p className='txtmvcart nameCmv'>{item.name}</p>
                    <p className='txtmvcart priceCmv'>₹{item.price}</p>
                    <p className='txtmvcart'>Clour : </p>
                    <p className='txtmvcart'>In Stock</p>
                    <p className='txtmvcart'>Convenience Fee: ₹45</p>

                    <div className='totalDivcartMV'>
                      <p className='tttxtcartMv txtmvcart'>Total :</p>
                      <p className='ttcartMv txtmvcart'>
                        ₹{item.qty * parseFloat(item.price)}
                      </p>
                    </div>
                  </div>
                </div>
              ))}
            </div>

            <hr />

            <div className='totalAmtFinalMv'>
              Total Amount{" "}
              <span>
                ₹
                {cartItems.reduce(
                  (acc, currVal) => acc + currVal.price * currVal.qty,
                  0
                ) -
                  parseInt(
                    cartItems.reduce(
                      (acc, currVal) => acc + currVal.price * currVal.qty,
                      0
                    ) *
                      (10 / 100)
                  ) +
                  45}
              </span>
            </div>

            <button
              className='btnmvcart'
              onClick={checkoutHandler}>
              PLACE ORDER
            </button>
          </div>
        </>
      )}
    </>
  ) : (
    <>
      <WebHeadingNCartBtn route={"/View Cart"} />

      <div className='cartScreenDviewMain'>
        <button
          className='backBtn'
          onClick={() => navigate("/")}>
          Back to products
        </button>

        <center style={{ marginTop: "2rem" }}>
          <img
            src={cartLog}
            alt='!'
            style={{ height: "38.741px" }}
          />
          <p>My Cart</p>
        </center>

        {cartItems.length === 0 ? (
          <>
            <ToastContainer />
          </>
        ) : (
          <>
            <div className='cartDGrid'>
              <div className='cartitemDesk'>
                {cartItems.map((item) => (
                  <div
                    className='ListOfProducts'
                    key={item.product}>
                    <div className='ImgDiv'>
                      <img
                        src={item.img}
                        alt='img'
                        style={{
                          height: "200px",
                          width: "200px",
                        }}
                      />
                    </div>

                    <div className='Pdtls1'>
                      <p className='Pdtls1_name'>{item.name}</p>
                      <p className='Pdtls1_color'>Black | Over-ear headphone</p>
                      <p className='Pdtls1_inStock'>In Stock</p>
                    </div>

                    <div className='Pdtls2'>
                      <p>Price</p>
                      <p>₹ {item.price}</p>
                    </div>

                    <div className='Pdtls3'>
                      <p>Quantity</p>
                      <select
                        name='qty'
                        id='qty'
                        value={item.qty}
                        onChange={(e) =>
                          dispatch(addToCart(item.product, +e.target.value))
                        }>
                        {[...Array(item.countInStock).keys()].map((i) => {
                          return (
                            <option
                              key={i + 1}
                              value={i + 1}>
                              {i + 1}
                            </option>
                          );
                        })}
                      </select>
                    </div>

                    <div className='Pdtls4'>
                      <p>Total</p>
                      <p>₹{item.qty * parseFloat(item.price)}</p>
                    </div>
                    <button
                      className='RemoveBtn'
                      onClick={() => removeFromCartHandler(item.product)}>
                      Remove
                    </button>
                  </div>
                ))}
              </div>

              <div className='totalDes'>
                <p className='totalDestxt ttHD'>PRICE DETAILS</p>

                <div className='infoDivtxtDesk'>
                  <p className='infoDivtxtDesktxt1 totalDestxt'>Total MRP</p>
                  <p className='infoDivtxtDesktxt2 totalDestxt'>
                    ₹
                    {cartItems.reduce(
                      (acc, currVal) => acc + currVal.price * currVal.qty,
                      0
                    )}
                  </p>
                </div>

                <div className='infoDivtxtDesk'>
                  <p className='infoDivtxtDesktxt1 totalDestxt'>
                    Discount on MRP
                  </p>
                  <p className='infoDivtxtDesktxt2 totalDestxt'>
                    - ₹
                    {parseInt(
                      cartItems.reduce(
                        (acc, currVal) => acc + currVal.price * currVal.qty,
                        0
                      ) *
                        (10 / 100)
                    )}
                  </p>
                </div>

                <div className='infoDivtxtDesk'>
                  <p className='infoDivtxtDesktxt1 totalDestxt'>
                    Convenience Fee
                  </p>
                  <p className='infoDivtxtDesktxt2 totalDestxt'>₹45</p>
                </div>

                <div className='infoDivtxtDesk finalttDesk'>
                  <p className='infoDivtxtDesktxt1 totalDestxt finalTAdesk'>
                    Total Amount
                  </p>
                  <p className='infoDivtxtDesktxt2 totalDestxt finalTAdesk'>
                    ₹
                    {cartItems.reduce(
                      (acc, currVal) => acc + currVal.price * currVal.qty,
                      0
                    ) -
                      parseInt(
                        cartItems.reduce(
                          (acc, currVal) => acc + currVal.price * currVal.qty,
                          0
                        ) *
                          (10 / 100)
                      ) +
                      45}
                  </p>
                </div>

                <button
                  className='placeOrderBtn'
                  onClick={checkoutHandler}>
                  PLACE ORDER
                </button>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
}

CartScreen.propTypes = {
  isMobile: PropTypes.bool.isRequired,
};

export default CartScreen;
