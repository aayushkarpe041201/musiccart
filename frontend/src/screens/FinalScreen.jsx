import "../css/FinalScreen.css";
import confetti from "/public/images/confetti 1.png";
import { useNavigate } from "react-router-dom";

function FinalScreen() {
  let navigate = useNavigate();
  return (
    <div className='mainFinalScreenDiv'>
      <img
        src={confetti}
        alt='Img'
      />

      <p className='textDeco1'>Order is placed successfully!</p>
      <p className='textDeco2'>
        You will be receiving a confirmation email with order details
      </p>

      <button onClick={() => navigate("/")}>Go back to Home page</button>
    </div>
  );
}

export default FinalScreen;
