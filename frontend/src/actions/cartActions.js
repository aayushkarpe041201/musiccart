import axios from "axios";

import {
  CART_ADD_ITEM,
  CART_REMOVE_ITEM,
  CART_PLACE_ORDER,
} from "../constants/cartConstants";

export const addToCart = (id, qty) => async (dispatch, getState) => {
  const { data } = await axios.get(`http://localhost:5000/api/products/${id}`);

  dispatch({
    type: CART_ADD_ITEM,
    payload: {
      product: data._id,
      name: data.name,
      price: data.Price,
      countInStock: data.qty,
      img: data.mainImg,
      imgList: [data.mainImg, ...data.imgList],
      qty,
    },
  });

  localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
};

export const removeFromCart = (id) => (dispatch, getState) => {
  dispatch({ type: CART_REMOVE_ITEM, payload: id });

  localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
};

export const placeOrder = () => (dispatch, getState) => {
  /// Make cart empty

  dispatch({ type: CART_PLACE_ORDER, payload: {} });
  localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
};
