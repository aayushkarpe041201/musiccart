import "../css/LoginScreen.css";
import Logo from "/images/image 4.png";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Loder from "../loder/Loder";
import { useState } from "react";
import { useEffect } from "react";
import { register } from "../actions/userAction";

function RegistationScreen() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const redirect = searchParams.get("redirect") || "/";

  const userRegister = useSelector((state) => state.userRegister);
  const { loading, error, userInfo } = userRegister;

  const [formData, setFormData] = useState({
    email: "",
    password: "",
    name: "",
    number: "",
  });

  const handleChange = (e) => {
    const { value, name } = e.target;

    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };

  const formHandler = (e) => {
    e.preventDefault();
    dispatch(
      register(
        formData.email,
        formData.password,
        formData.name,
        formData.number
      )
    );
  };

  useEffect(() => {
    if (userInfo) {
      navigate(redirect);
    } else if (error) {
      toast.error(error);
      setFormData({
        email: "",
        password: "",
        number: "",
        name: "",
      });
    }
  }, [navigate, userInfo, redirect, error]);

  return (
    <>
      {loading ? (
        <>
          <Loder />
        </>
      ) : (
        <>
          <div className='LoginScreenMainDiv'>
            <div className='HeadingDiv'>
              <img
                src={Logo}
                alt='LOgo'
              />
              <h1>Musicart</h1>
            </div>
            <div className='SignIndiv boxConatainer'>
              <h3>Create Account</h3>
              <label htmlFor='name'>Your name</label>
              <input
                type='name'
                name='name'
                className='boxConatainerInp email number'
                onBlur={handleChange}
              />
              <label htmlFor='number'>Mobile number</label>
              <input
                type='number'
                name='number'
                className='boxConatainerInp Password'
                onBlur={handleChange}
              />
              <label htmlFor='email'>Email Id</label>
              <input
                type='email'
                name='email'
                className='boxConatainerInp Password'
                onBlur={handleChange}
              />
              <label htmlFor='password'>Password</label>
              <input
                type='password'
                name='password'
                className='boxConatainerInp Password'
                onBlur={handleChange}
              />
              <p>
                By enrolling your mobile phone number, you consent to receive
                automated <br />
                security notifications via text message from Musicart. Message
                and data
                <br /> rates may apply.
              </p>

              <button onClick={formHandler}>Continue</button>
              <p>
                By continuing, you agree to Musicart privacy notice and
                conditions of use.
              </p>
            </div>
            <div className='newUser'>
              <hr />
              <p>Already have an account?</p>
              <hr />
            </div>
            <button className='regBtn'>Sign in</button>

            <ToastContainer />
          </div>
        </>
      )}
    </>
  );
}

export default RegistationScreen;
