import PropTypes from "prop-types";
import "../css/HomeScreen.css";
import Ads from "../components/Ads";
import { useDispatch, useSelector } from "react-redux";

import GridCard from "../components/GridCard";
import ListCard from "../components/ListCard";
import { useState } from "react";
import WebHeadingNCartBtn from "../components/WebHeadingNCartBtn";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Loder from "../loder/Loder";

import { listProducts } from "../actions/productAction";
import { useEffect } from "react";
import { Link } from "react-router-dom";

function HomeScreen({ isMobile }) {
  const dispatch = useDispatch();
  const [isGrid, setIsGrid] = useState(true);

  const productList = useSelector((state) => state.productList);
  const { loading, error, products } = productList;

  const handleGrid = (type) => {
    type === "list" ? setIsGrid(false) : setIsGrid(true);
  };

  useEffect(() => {
    dispatch(listProducts());

    if (error) {
      toast.error(error);
    }
  }, [dispatch, error]);

  const [searchData, setSearchData] = useState([]);

  const handleSeach = (e) => {
    const { value } = e.target;

    let newArrmap = products.map((item) => ({
      id: item._id,
      name: item.name,
    }));

    let newArrfilter = newArrmap.filter(
      (item) =>
        item.name &&
        value &&
        item.name.toLowerCase().includes(value.toLowerCase())
    );

    setSearchData(newArrfilter);
  };

  const [filterValues, setFilterValues] = useState({
    type_: "",
    company_: "",
    color_: "",
    price_: "",
    featured_: "",
  });

  const handleFilter = (e) => {
    const { name, value } = e.target;

    setFilterValues((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  // Function to filter and sort products based on the selected filter values
  const filterAndSortProducts = (products, filterValues) => {
    let filteredProducts = [...products];

    // Filter based on type
    if (filterValues.type_) {
      filteredProducts = filteredProducts.filter(
        (product) => product.type === filterValues.type_
      );
    }

    // Filter based on company
    if (filterValues.company_) {
      filteredProducts = filteredProducts.filter(
        (product) => product.Brand === filterValues.company_
      );
    }

    // Filter based on color
    if (filterValues.color_) {
      filteredProducts = filteredProducts.filter(
        (product) => product.color === filterValues.color_
      );
    }

    // Filter based on price range
    if (filterValues.price_) {
      const [minPrice, maxPrice] = filterValues.price_.split(" - ");
      filteredProducts = filteredProducts.filter(
        (product) =>
          product.Price >= parseInt(minPrice) &&
          product.Price <= parseInt(maxPrice)
      );
    }

    // Handle sorting based on featured_
    if (filterValues.featured_) {
      switch (filterValues.featured_) {
        case "Lowest":
          filteredProducts.sort((a, b) => a.Price - b.Price);
          break;
        case "Highest":
          filteredProducts.sort((a, b) => b.Price - a.Price);
          break;
        case "A":
          filteredProducts.sort((a, b) => a.name.localeCompare(b.name));
          break;
        case "Z":
          filteredProducts.sort((a, b) => b.name.localeCompare(a.name));
          break;
        default:
          // Do nothing for other cases
          break;
      }
    }

    return filteredProducts;
  };

  const filteredAndSortedProducts = filterAndSortProducts(
    products,
    filterValues
  );

  return (
    <>
      {loading ? (
        <>
          <WebHeadingNCartBtn route={"/Loading..."} />
          <Loder />
        </>
      ) : error ? (
        <>
          <ToastContainer />
        </>
      ) : (
        <>
          <WebHeadingNCartBtn route={"/"} />
          <div className='HomeScreenMainDiv'>
            <Ads />
            <div
              className='searchBar'
              style={isMobile ? { display: "none" } : {}}>
              <input
                type='text'
                placeholder='Search Product'
                onChange={handleSeach}
                style={searchData.length === 0 ? {} : { marginBottom: "0rem" }}
              />
              <div
                className='filterSearchBarDatadesk'
                style={
                  searchData.length === 0
                    ? { display: "none" }
                    : { display: "flex" }
                }>
                {searchData.map((item) => (
                  <Link
                    to={`/singleproductscreen/${item.id}`}
                    key={item.id}>
                    <p className='filterSearchBarDatadeskP'>{item.name}</p>
                  </Link>
                ))}
              </div>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                width='32'
                height='32'
                viewBox='0 0 47 47'
                fill='none'
                className='searchIcon'>
                <path
                  d='M20.2954 5.875C17.4433 5.875 14.6553 6.72074 12.2838 8.30527C9.91243 9.8898 8.06414 12.142 6.97269 14.7769C5.88125 17.4119 5.59568 20.3114 6.15209 23.1086C6.7085 25.9059 8.08191 28.4754 10.0986 30.4921C12.1154 32.5088 14.6848 33.8822 17.4821 34.4387C20.2794 34.9951 23.1788 34.7095 25.8138 33.6181C28.4488 32.5266 30.7009 30.6783 32.2855 28.3069C33.87 25.9355 34.7157 23.1475 34.7157 20.2954C34.7155 16.4709 33.1961 12.8032 30.4919 10.0989C27.7876 7.39461 24.1198 5.87524 20.2954 5.875Z'
                  stroke='#666666'
                  strokeWidth='2.9375'
                  strokeMiterlimit='10'
                />
                <path
                  d='M31.0537 31.0537L41.1247 41.1247'
                  stroke='#666666'
                  strokeWidth='2.9375'
                  strokeMiterlimit='10'
                  strokeLinecap='round'
                />
              </svg>
            </div>

            <div className='filterMianDiv'>
              <div style={{ display: "flex" }}>
                {!isMobile && (
                  <div className='gridOrListDiv'>
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      width='29'
                      height='30'
                      viewBox='0 0 49 50'
                      fill='none'>
                      <path
                        d='M10.1854 22.963C9.06504 22.963 8.10559 22.5638 7.30707 21.7653C6.50855 20.9668 6.10997 20.008 6.11133 18.889V10.7408C6.11133 9.62046 6.51059 8.66101 7.30911 7.86249C8.10763 7.06397 9.06639 6.66539 10.1854 6.66675H18.3336C19.4539 6.66675 20.4134 7.06601 21.2119 7.86453C22.0104 8.66305 22.409 9.62181 22.4076 10.7408V18.889C22.4076 20.0093 22.0084 20.9688 21.2099 21.7673C20.4113 22.5658 19.4526 22.9644 18.3336 22.963H10.1854ZM10.1854 43.3334C9.06504 43.3334 8.10559 42.9342 7.30707 42.1356C6.50855 41.3371 6.10997 40.3784 6.11133 39.2593V31.1112C6.11133 29.9908 6.51059 29.0314 7.30911 28.2329C8.10763 27.4343 9.06639 27.0358 10.1854 27.0371H18.3336C19.4539 27.0371 20.4134 27.4364 21.2119 28.2349C22.0104 29.0334 22.409 29.9922 22.4076 31.1112V39.2593C22.4076 40.3797 22.0084 41.3392 21.2099 42.1377C20.4113 42.9362 19.4526 43.3348 18.3336 43.3334H10.1854ZM30.5558 22.963C29.4354 22.963 28.476 22.5638 27.6774 21.7653C26.8789 20.9668 26.4803 20.008 26.4817 18.889V10.7408C26.4817 9.62046 26.881 8.66101 27.6795 7.86249C28.478 7.06397 29.4368 6.66539 30.5558 6.66675H38.7039C39.8243 6.66675 40.7837 7.06601 41.5823 7.86453C42.3808 8.66305 42.7794 9.62181 42.778 10.7408V18.889C42.778 20.0093 42.3787 20.9688 41.5802 21.7673C40.7817 22.5658 39.8229 22.9644 38.7039 22.963H30.5558ZM30.5558 43.3334C29.4354 43.3334 28.476 42.9342 27.6774 42.1356C26.8789 41.3371 26.4803 40.3784 26.4817 39.2593V31.1112C26.4817 29.9908 26.881 29.0314 27.6795 28.2329C28.478 27.4343 29.4368 27.0358 30.5558 27.0371H38.7039C39.8243 27.0371 40.7837 27.4364 41.5823 28.2349C42.3808 29.0334 42.7794 29.9922 42.778 31.1112V39.2593C42.778 40.3797 42.3787 41.3392 41.5802 42.1377C40.7817 42.9362 39.8229 43.3348 38.7039 43.3334H30.5558Z'
                        fill='black'
                        onClick={() => handleGrid("grid")}
                      />
                    </svg>
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      width='31'
                      height='30'
                      viewBox='0 0 61 60'
                      fill='none'>
                      <path
                        d='M52.9443 15.0004C52.946 14.2937 52.7068 13.7004 52.2268 13.2204C51.7468 12.7404 51.1527 12.5004 50.4443 12.5004H22.9443C22.2377 12.4987 21.6443 12.7379 21.1643 13.2179C20.6843 13.6979 20.4443 14.292 20.4443 15.0004H52.9443ZM52.9443 15.0004V20.0004C52.9443 20.7087 52.7043 21.3029 52.2243 21.7829C51.7443 22.2629 51.151 22.502 50.4443 22.5004L52.9443 15.0004ZM8.72212 20.0022V20.0004V15.0004C8.72212 14.4883 8.8871 14.0951 9.21432 13.7678C9.54144 13.4407 9.93328 13.2769 10.4425 13.2782V13.2782H10.4443H15.4443C15.9564 13.2782 16.3497 13.4431 16.6769 13.7703C17.004 14.0975 17.1678 14.4893 17.1666 14.9985V15.0004V20.0004C17.1666 20.5124 17.0016 20.9057 16.6744 21.2329C16.3472 21.56 15.9554 21.7238 15.4462 21.7226H15.4443H10.4443C9.93229 21.7226 9.53903 21.5576 9.21182 21.2304L8.66966 21.7726L9.21182 21.2304C8.88469 20.9033 8.72092 20.5114 8.72212 20.0022ZM21.2221 20.0022V20.0004V15.0004C21.2221 14.4883 21.3871 14.0951 21.7143 13.7678C22.0414 13.4407 22.4333 13.2769 22.9425 13.2782H22.9443H50.4443C50.9564 13.2782 51.3497 13.4431 51.6769 13.7703C52.004 14.0975 52.1678 14.4893 52.1666 14.9985V15.0004V20.0004C52.1666 20.5124 52.0016 20.9057 51.6744 21.2329C51.3472 21.56 50.9554 21.7238 50.4462 21.7226H50.4443H22.9443C22.4323 21.7226 22.039 21.5576 21.7118 21.2304C21.3847 20.9033 21.2209 20.5114 21.2221 20.0022ZM21.2221 32.5022V32.5004V27.5004C21.2221 26.9883 21.3871 26.5951 21.7143 26.2678C22.0414 25.9407 22.4333 25.7769 22.9425 25.7782H22.9443H50.4443C50.9564 25.7782 51.3497 25.9431 51.6769 26.2703C52.004 26.5975 52.1678 26.9893 52.1666 27.4985V27.5004V32.5004C52.1666 33.0124 52.0016 33.4057 51.6744 33.7329C51.3472 34.06 50.9554 34.2238 50.4462 34.2226H50.4443H22.9443C22.4323 34.2226 22.039 34.0576 21.7118 33.7304C21.3847 33.4033 21.2209 33.0114 21.2221 32.5022ZM8.72212 32.5022V32.5004V27.5004C8.72212 26.9883 8.8871 26.5951 9.21432 26.2678L8.66434 25.7179L9.21432 26.2678C9.54144 25.9407 9.93328 25.7769 10.4425 25.7782H10.4443H15.4443C15.9564 25.7782 16.3497 25.9431 16.6769 26.2703C17.004 26.5975 17.1678 26.9893 17.1666 27.4985V27.5004V32.5004C17.1666 33.0124 17.0016 33.4057 16.6744 33.7329C16.3472 34.06 15.9554 34.2238 15.4462 34.2226H15.4443H10.4443C9.93229 34.2226 9.53903 34.0576 9.21182 33.7304C8.88469 33.4033 8.72092 33.0114 8.72212 32.5022ZM21.2221 45.0022V45.0004V40.0004C21.2221 39.4883 21.3871 39.0951 21.7143 38.7679C22.0414 38.4407 22.4333 38.277 22.9425 38.2782H22.9443H50.4443C50.9564 38.2782 51.3497 38.4431 51.6769 38.7704C52.004 39.0975 52.1678 39.4893 52.1666 39.9985V40.0004V45.0004C52.1666 45.5124 52.0016 45.9057 51.6744 46.2329C51.3472 46.56 50.9554 46.7238 50.4462 46.7226H50.4443H22.9443C22.4323 46.7226 22.039 46.5576 21.7118 46.2304C21.3847 45.9033 21.2209 45.5114 21.2221 45.0022ZM8.72212 45.0022V45.0004V40.0004C8.72212 39.4883 8.8871 39.0951 9.21432 38.7679L8.67113 38.2247L9.21432 38.7678C9.54144 38.4407 9.93328 38.277 10.4425 38.2782H10.4443H15.4443C15.9564 38.2782 16.3497 38.4431 16.6769 38.7704C17.004 39.0975 17.1678 39.4893 17.1666 39.9985V40.0004V45.0004C17.1666 45.5124 17.0016 45.9057 16.6744 46.2329C16.3472 46.56 15.9554 46.7238 15.4462 46.7226H15.4443H10.4443C9.93229 46.7226 9.53903 46.5576 9.21182 46.2304C8.88469 45.9033 8.72092 45.5114 8.72212 45.0022ZM20.4443 32.5004V27.5004C20.4443 26.792 20.6843 26.1979 21.1643 25.7179C21.6443 25.2379 22.2377 24.9987 22.9443 25.0004L20.4443 32.5004Z'
                        fill='white'
                        stroke='black'
                        strokeWidth='1.55556'
                        onClick={() => handleGrid("list")}
                      />
                    </svg>
                  </div>
                )}

                <div
                  className='filtersContainers'
                  style={isMobile ? { marginTop: "1.5rem" } : {}}>
                  <select
                    name='type_'
                    id='type_'
                    onChange={(e) => handleFilter(e)}>
                    <option value=''>Headphone type </option>
                    <option value='In-ear headphone'>In-ear headphone </option>
                    <option value='On-ear headphone'>On-ear headphone</option>
                    <option value='Over-ear headphone'>
                      Over-ear headphone
                    </option>
                  </select>

                  <select
                    name='company_'
                    id='company_'
                    onChange={(e) => handleFilter(e)}>
                    <option value=''>Company </option>
                    <option value='Oneplus'>Oneplus </option>
                    <option value='Sony'>Sony </option>
                    <option value='Boat'>Boat </option>
                    <option value='realme'>realme </option>
                  </select>

                  <select
                    name='color_'
                    id='color_'
                    onChange={(e) => handleFilter(e)}>
                    <option value=''>Color </option>
                    <option value='Blue'>Blue </option>
                    <option value='Black'>Black </option>
                    <option value='White'>White </option>
                    <option value='Green'>Green </option>
                  </select>

                  <select
                    name='price_'
                    id='price_'
                    onChange={(e) => handleFilter(e)}>
                    <option value=''>Price </option>
                    <option value='0 - 1000'>₹0 - ₹1,000 </option>
                    <option value='1000 - 10000'>₹1,000 - ₹10,000 </option>
                    <option value='10000 - 20000'>₹10,000 - ₹20,000 </option>
                  </select>
                </div>
              </div>

              <select
                name='featured_'
                id='featured_'
                onChange={(e) => handleFilter(e)}>
                <option value=''>Sort by : Featured </option>
                <option value='Lowest'>Price : Lowest </option>
                <option value='Highest'>Price : Highest </option>
                <option value='A'>Name : (A-Z) </option>
                <option value='Z'>Name : (Z-A) </option>
              </select>
            </div>

            <>
              {isGrid ? (
                <div className='productListDiv'>
                  {filteredAndSortedProducts.map((product) => (
                    <GridCard
                      product={product}
                      //filterValues={filterValues}
                      key={product._id}
                    />
                  ))}
                </div>
              ) : (
                <div className='productListDiv2'>
                  {filteredAndSortedProducts.map((product) => (
                    <ListCard
                      key={product._id}
                      //filterValues={filterValues}
                      product={product}
                    />
                  ))}
                </div>
              )}
            </>
          </div>
        </>
      )}
    </>
  );
}

HomeScreen.propTypes = {
  isMobile: PropTypes.bool.isRequired,
};

export default HomeScreen;
