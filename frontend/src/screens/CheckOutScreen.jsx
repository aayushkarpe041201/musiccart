import { useNavigate } from "react-router-dom";
import "../css/CheckOutScreen.css";
import WebHeadingNCartBtn from "../components/WebHeadingNCartBtn";
import MainImg from "/images/image 23.jpg";
import { useDispatch, useSelector } from "react-redux";
import { placeOrder } from "../actions/cartActions";

function CheckOutScreen() {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;
  let navigate = useNavigate();

  const handlePlaceOrder = () => {
    dispatch(placeOrder());
    navigate("/finalscreen");
  };
  return (
    <>
      <WebHeadingNCartBtn route={`/Checkout`} />
      <div className='checkOSMainDiv'>
        <button
          className='backBtnC'
          onClick={() => navigate("/cart")}>
          Back to products
        </button>

        <center style={{ marginTop: "2rem" }}>
          <p>My Cart</p>
        </center>

        <div className='checkOSsubDiv'>
          <div className='steps'>
            <div className='addSec fl'>
              <p className='txtRed'>1. Delivery address</p>
              <p className='txtgray'>
                Ayush karpe 104 kk hh nagar, Lucknow Uttar Pradesh 226025
              </p>
            </div>
            <hr />
            <div className='fl'>
              <p className='txtRed'>2. Payment method</p>
              <p className='txtgray'>Pay on delivery (Cash/Card)</p>
            </div>
            <hr />
            <div className='dtlsSec fl'>
              <p className='txtRed'>3. Review items and delivery</p>
              <div className='gridProdContain'>
                {cartItems.map((item) => (
                  <div
                    className='productDiv'
                    key={item.product}>
                    <div className='imgDivP'>
                      <img
                        src={item.img}
                        alt='!!'
                      />
                    </div>
                    <p className='txtName'>{item.name}</p>
                    <p className='txtgray'>Clour : Black</p>
                    <p className='txtgray'>In Stock</p>
                    <p className='txtEstTime'>Estimated delivery : </p>
                    <p className='txtEstTime'>
                      Monday — FREE Standard Delivery{" "}
                    </p>
                  </div>
                ))}
              </div>
            </div>
            <hr />

            <div className='placeOrderDiv'>
              <button onClick={handlePlaceOrder}>Place your order</button>
              <div className='placeOrderDivtxt'>
                <p className='txtRed'>
                  {" "}
                  Order Total : ₹
                  {cartItems.reduce(
                    (acc, currVal) => acc + currVal.price * currVal.qty,
                    0
                  ) -
                    parseInt(
                      cartItems.reduce(
                        (acc, currVal) => acc + currVal.price * currVal.qty,
                        0
                      ) *
                        (10 / 100)
                    )}
                </p>
                <p className='txtgray'>
                  By placing your order, you agree to Musicart privacy notice
                  and conditions of use.
                </p>
              </div>
            </div>
          </div>
          <div className='totaldiv'>
            <div className='summaryDiv'>
              <button onClick={handlePlaceOrder}>Place your order</button>
              <p>By placing your order, you agree to Musicart privacy</p>
              <p>notice and conditions of use.</p>
              <hr />
              <p className='txtH'>Order Summary</p>

              <div className='itemsPdiv'>
                <p>Items :</p>
                <p>
                  ₹
                  {cartItems.reduce(
                    (acc, currVal) => acc + currVal.price * currVal.qty,
                    0
                  ) -
                    parseInt(
                      cartItems.reduce(
                        (acc, currVal) => acc + currVal.price * currVal.qty,
                        0
                      ) *
                        (10 / 100)
                    )}
                </p>
              </div>
              <div className='itemsPdiv'>
                <p>Delivery :</p>
                <p>₹45.00</p>
              </div>
              <hr />
              <div className='f2'>
                <p>Order Total :</p>
                <p>
                  ₹{" "}
                  {cartItems.reduce(
                    (acc, currVal) => acc + currVal.price * currVal.qty,
                    0
                  ) -
                    parseInt(
                      cartItems.reduce(
                        (acc, currVal) => acc + currVal.price * currVal.qty,
                        0
                      ) *
                        (10 / 100)
                    ) +
                    45}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default CheckOutScreen;
