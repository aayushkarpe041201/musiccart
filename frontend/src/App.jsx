import "./css/App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Footer from "./components/Footer";
import LoginScreen from "./screens/LoginScreen.jsx";
import RegistationScreen from "./screens/RegistationScreen";
import Header from "./components/Header";
import HomeScreen from "./screens/HomeScreen";
import { useEffect, useState } from "react";
import Loder from "./loder/Loder";
import SingleProductScreen from "./screens/SingleProductScreen";
import FinalScreen from "./screens/FinalScreen";
import CartScreen from "./screens/CartScreen";
import CheckOutScreen from "./screens/CheckOutScreen";

function App() {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <BrowserRouter>
      <Header isMobile={isMobile} />
      <div
        className='main'
        style={{
          width: "auto",
          height: "100vh",
          margin: "1rem 2.5rem 2rem 2.5rem",
        }}>
        <Routes>
          <Route
            path='/'
            element={<HomeScreen isMobile={isMobile} />}
          />
          <Route
            path='/loder'
            element={<Loder />}
          />
          <Route
            path='/login'
            element={<LoginScreen />}
          />
          <Route
            path='/register'
            element={<RegistationScreen />}
          />
          <Route
            path='/finalscreen'
            element={<FinalScreen />}
          />
          <Route
            path='/checkout'
            element={<CheckOutScreen />}
          />
          <Route
            path='/cart'
            element={<CartScreen isMobile={isMobile} />}
          />
          <Route
            path='/cart/:id'
            element={<CartScreen isMobile={isMobile} />}
          />
          <Route
            path='/singleproductscreen/:id'
            element={<SingleProductScreen isMobile={isMobile} />}
          />
        </Routes>
      </div>
      <Footer isMobile={isMobile} />
    </BrowserRouter>
  );
}

export default App;
