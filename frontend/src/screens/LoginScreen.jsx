import Logo from "/images/image 4.png";
import "../css/LoginScreen.css";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useSearchParams } from "react-router-dom";
import { useState } from "react";
import { useEffect } from "react";
import { login } from "../actions/userAction";
import Loder from "../loder/Loder";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function LoginScreen() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [searchParams] = useSearchParams();
  let redirect = searchParams.get("redirect") || "/";

  const [form, setForm] = useState({
    email: "",
    password: "",
  });

  const userLogin = useSelector((state) => state.userLogin);
  const { loading, error, userInfo } = userLogin;

  useEffect(() => {
    if (userInfo) {
      navigate(redirect);
    }
  }, [navigate, userInfo, redirect]);

  const submitHandler = (e) => {
    e.preventDefault();
    //console.log(form);
    dispatch(login(form.email, form.password));
  };

  const handleOnblur = (e) => {
    const { value, name } = e.target;

    setForm((prev) => ({ ...prev, [name]: value }));
  };

  useEffect(() => {
    if (userInfo) {
      navigate(redirect);
    } else if (error) {
      toast.error(error);
      setForm({
        email: "",
        password: "",
      });
    }
  }, [navigate, userInfo, redirect, error, loading]);

  return (
    <>
      {loading ? (
        <>
          <Loder />
        </>
      ) : (
        <>
          <div className='LoginScreenMainDiv'>
            <div className='HeadingDiv'>
              <img
                src={Logo}
                alt='LOgo'
              />
              <h1>Musicart</h1>
            </div>
            <div className='SignIndiv boxConatainer'>
              <h3>Sign in</h3>
              <label htmlFor='email'>Enter your email or mobile number</label>
              <input
                type='email'
                className='boxConatainerInp email number'
                name='email'
                onBlur={handleOnblur}
              />
              <label htmlFor='Password'>Password</label>
              <input
                type='password'
                name='password'
                className='boxConatainerInp Password'
                onBlur={handleOnblur}
              />

              <button onClick={submitHandler}>Continue</button>
              <p>
                By continuing, you agree to Musicart privacy notice and
                conditions of use.
              </p>
            </div>
            <div className='newUser'>
              <hr />
              <p>New to Musicart?</p>
              <hr />
            </div>
            <button className='regBtn'>Create your Musicart account</button>

            <ToastContainer />
          </div>
        </>
      )}
    </>
  );
}

export default LoginScreen;
