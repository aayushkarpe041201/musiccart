import { useLocation } from "react-router-dom";
import "../css/Footer.css";
import PropTypes from "prop-types";
import { useState, useEffect } from "react";
import { logout } from "../actions/userAction";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

function Footer({ isMobile }) {
  const location = useLocation();
  const [route, setroute] = useState(location.pathname);

  useEffect(() => {
    setroute(location.pathname);
  }, [location.pathname]);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const logoutHandler = () => {
    dispatch(logout());
    navigate("/");
  };

  if (
    ((route === "/login" || route === "/register") && isMobile) ||
    !isMobile
  ) {
    return (
      <div
        style={{
          zIndex: "5",
          width: "100%",
          backgroundColor: "#2E0052",
          fontFamily: "Roboto",
          fontSize: "1rem",
          fontWeight: "500",
          lineHeight: "31px",
          letterSpacing: "0em",
          textAlign: "center",
          color: "#FFFFFF",
          position: "fixed",
          bottom: "0",
        }}>
        Musicart | All rights reserved
      </div>
    );
  } else if ((route !== "/login" || route !== "/register") && isMobile) {
    return (
      <div className='mobileViewFooter'>
        <Link to={"/"}>
          <div className='mvhomediv contMV'>
            <svg
              style={route !== "/cart" ? {} : { visibility: "hidden" }}
              xmlns='http://www.w3.org/2000/svg'
              width='31'
              height='4'
              viewBox='0 0 31 4'
              fill='none'>
              <path
                d='M2 2H29'
                stroke='#2E0052'
                strokeWidth='4'
                strokeLinecap='round'
              />
            </svg>

            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='21'
              height='24'
              viewBox='0 0 21 24'
              fill='none'>
              <path
                d='M6.875 22.625H1V8.375L10.5 1.25L20 8.375V22.625H14.125V15.0375C14.125 14.1538 13.4087 13.4375 12.525 13.4375H8.475C7.59134 13.4375 6.875 14.1538 6.875 15.0375V22.625Z'
                fill='white'
                stroke='#2E0052'
                strokeWidth='2'
              />
            </svg>

            <p>Home</p>
          </div>
        </Link>

        <Link to={"/cart"}>
          <div className='mvCartdiv contMV'>
            <svg
              style={route === "/cart" ? {} : { visibility: "hidden" }}
              xmlns='http://www.w3.org/2000/svg'
              width='31'
              height='4'
              viewBox='0 0 31 4'
              fill='none'>
              <path
                d='M2 2H29'
                stroke='#2E0052'
                strokeWidth='4'
                strokeLinecap='round'
              />
            </svg>

            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='24'
              height='25'
              viewBox='0 0 24 25'
              fill='none'>
              <path
                d='M11.5079 9.20635H13.8095V5.75397H17.2619V3.45238H13.8095V0H11.5079V3.45238H8.05556V5.75397H11.5079V9.20635ZM6.90476 19.5635C5.63889 19.5635 4.61468 20.5992 4.61468 21.8651C4.61468 23.131 5.63889 24.1667 6.90476 24.1667C8.17064 24.1667 9.20635 23.131 9.20635 21.8651C9.20635 20.5992 8.17064 19.5635 6.90476 19.5635ZM18.4127 19.5635C17.1468 19.5635 16.1226 20.5992 16.1226 21.8651C16.1226 23.131 17.1468 24.1667 18.4127 24.1667C19.6786 24.1667 20.7143 23.131 20.7143 21.8651C20.7143 20.5992 19.6786 19.5635 18.4127 19.5635ZM7.1004 15.8234L7.13492 15.6853L8.17064 13.8095H16.744C17.6071 13.8095 18.3667 13.3377 18.7579 12.6242L23.2 4.55714L21.1976 3.45238H21.1861L19.9202 5.75397L16.744 11.5079H8.66548L8.51587 11.1972L5.9381 5.75397L4.84484 3.45238L3.7631 1.15079H0V3.45238H2.30159L6.44444 12.1869L4.89087 15.0063C4.70675 15.3286 4.60317 15.7083 4.60317 16.1111C4.60317 17.377 5.63889 18.4127 6.90476 18.4127H20.7143V16.1111H7.3881C7.23849 16.1111 7.1004 15.9845 7.1004 15.8234Z'
                fill='#2E0052'
              />
            </svg>

            <p>Cart</p>
          </div>
        </Link>

        {userInfo && (
          <div
            className='mvlogDiv contMV'
            onClick={logoutHandler}>
            <svg
              style={{ visibility: "hidden" }}
              xmlns='http://www.w3.org/2000/svg'
              width='31'
              height='7'
              viewBox='0 0 31 4'
              fill='none'>
              <path
                d='M2 2H29'
                stroke='#2E0052'
                strokeWidth='4'
                strokeLinecap='round'
              />
            </svg>

            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='20'
              height='20'
              viewBox='0 0 20 20'
              fill='none'>
              <path
                d='M9.9 11.1375C13.2041 11.1375 19.8 12.7957 19.8 16.0875V19.8H0V16.0875C0 12.7957 6.59587 11.1375 9.9 11.1375ZM9.9 0C11.2128 0 12.4719 0.521516 13.4002 1.44982C14.3285 2.37813 14.85 3.63718 14.85 4.95C14.85 6.26282 14.3285 7.52187 13.4002 8.45018C12.4719 9.37848 11.2128 9.9 9.9 9.9C8.58718 9.9 7.32813 9.37848 6.39982 8.45018C5.47152 7.52187 4.95 6.26282 4.95 4.95C4.95 3.63718 5.47152 2.37813 6.39982 1.44982C7.32813 0.521516 8.58718 0 9.9 0ZM9.9 13.4887C6.22462 13.4887 2.35125 15.2955 2.35125 16.0875V17.4488H17.4488V16.0875C17.4488 15.2955 13.5754 13.4887 9.9 13.4887ZM9.9 2.35125C9.21077 2.35125 8.54977 2.62505 8.06241 3.11241C7.57505 3.59977 7.30125 4.26077 7.30125 4.95C7.30125 5.63923 7.57505 6.30023 8.06241 6.78759C8.54977 7.27495 9.21077 7.54875 9.9 7.54875C10.5892 7.54875 11.2502 7.27495 11.7376 6.78759C12.225 6.30023 12.4988 5.63923 12.4988 4.95C12.4988 4.26077 12.225 3.59977 11.7376 3.11241C11.2502 2.62505 10.5892 2.35125 9.9 2.35125Z'
                fill='#2E0052'
              />
            </svg>

            <p>Logout</p>
          </div>
        )}

        {!userInfo && (
          <Link to={"/login"}>
            <div className='mvlogDiv contMV'>
              <svg
                style={{ visibility: "hidden" }}
                xmlns='http://www.w3.org/2000/svg'
                width='31'
                height='7'
                viewBox='0 0 31 4'
                fill='none'>
                <path
                  d='M2 2H29'
                  stroke='#2E0052'
                  strokeWidth='4'
                  strokeLinecap='round'
                />
              </svg>

              <svg
                xmlns='http://www.w3.org/2000/svg'
                width='20'
                height='20'
                viewBox='0 0 20 20'
                fill='none'>
                <path
                  d='M9.9 11.1375C13.2041 11.1375 19.8 12.7957 19.8 16.0875V19.8H0V16.0875C0 12.7957 6.59587 11.1375 9.9 11.1375ZM9.9 0C11.2128 0 12.4719 0.521516 13.4002 1.44982C14.3285 2.37813 14.85 3.63718 14.85 4.95C14.85 6.26282 14.3285 7.52187 13.4002 8.45018C12.4719 9.37848 11.2128 9.9 9.9 9.9C8.58718 9.9 7.32813 9.37848 6.39982 8.45018C5.47152 7.52187 4.95 6.26282 4.95 4.95C4.95 3.63718 5.47152 2.37813 6.39982 1.44982C7.32813 0.521516 8.58718 0 9.9 0ZM9.9 13.4887C6.22462 13.4887 2.35125 15.2955 2.35125 16.0875V17.4488H17.4488V16.0875C17.4488 15.2955 13.5754 13.4887 9.9 13.4887ZM9.9 2.35125C9.21077 2.35125 8.54977 2.62505 8.06241 3.11241C7.57505 3.59977 7.30125 4.26077 7.30125 4.95C7.30125 5.63923 7.57505 6.30023 8.06241 6.78759C8.54977 7.27495 9.21077 7.54875 9.9 7.54875C10.5892 7.54875 11.2502 7.27495 11.7376 6.78759C12.225 6.30023 12.4988 5.63923 12.4988 4.95C12.4988 4.26077 12.225 3.59977 11.7376 3.11241C11.2502 2.62505 10.5892 2.35125 9.9 2.35125Z'
                  fill='#2E0052'
                />
              </svg>

              <p>Login</p>
            </div>
          </Link>
        )}
      </div>
    );
  }
}

Footer.propTypes = {
  isMobile: PropTypes.bool.isRequired,
};

export default Footer;
